### TT Build Script

Набор ansible playbook'ов для сборки нужной версии Tarantool

## Использование

Необходим установленный Ansible

0) Добавляем адрес/адреса машин, на которых нужно собрать Tarantool, в ansible/hosts;

Машины должны быть доступны по ssh без пароля.
На машинах должна быть установлена CentOS 7

1) Устанавливаем на машине/машинах необходимые для сборки Tarantool зависимости (достаточно выполнить один раз)

```sh
cd ansible
ansible-playbook 00-prerequisites.yml
```

2) Открываем ```ansible/01-build_tt.yml``` любым текстовым редактором, в переменной ```tt_ver:``` указываем нужную версию Tarantool.
Версия это либо ветка, либо тэг, SHA-1 Hash нужного коммита

3)
```sh
cd ansible
ansible-playbook 01-build_tt.yml
```

