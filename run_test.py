import asyncio
import ssh_process

# bench_command="""
# cd bench && tarantool init_17.lua | tee & sleep 1 && kill $! & fg %1
# """

launch_tt_command="""
cd bench && tarantool init10.lua & echo $!
"""

launch_bench_command="""
bench/bench -w 8 & echo $!
"""


# bench_command="cd bench && tarantool init_17.lua"


async def async_main():
    tt, bench, *_ = await ssh_process.bulk('tt-build2', 'tt-build2')

    # tt_serv, bench_serv = sshs

    # ret_text = await tt_serv.communicate(bench_command)
    tt_pid, *_ = await tt.communicate(launch_tt_command)

    # пытаемся запустить бенс
    # bench_pid, *_ = await bench.communicate(launch_bench_command)
    bench_pid, *_ = await bench.communicate(launch_bench_command)

    
    # print(tt_pid)
    # print(bench_pid)

    # print(ret_text)
    # await asyncio.sleep(10)
    # await tt_serv.send('./tarantool --version')
    # sshs = await ssh_process.bulk('tt-build2', 'tt-build2')

    # await sshs[0].send('sleep 10 && echo wow')
    # await sshs[1].send('sleep 10 && echo ouchie')

    await asyncio.sleep(10)
    # print(await sshs[0].receive())
    # print(await sshs[1].receive())


asyncio.run(async_main())
