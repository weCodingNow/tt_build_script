VERSION="${1:-"1.10.5"}"

pushd ansible
ansible-playbook 01-build_tt.yml --extra-vars "tt_ver=\"$VERSION\""
popd
