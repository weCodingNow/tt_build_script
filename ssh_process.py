import typing
import asyncio

OUT_BUFFER_SIZE = 1600

def _get_sender(process):
    async def sender(msg: str):
        process.stdin.write(msg.encode() + b'\n')
        await process.stdin.drain()

    return sender

def _get_reader(process):
    async def reader():
        raw_out = await process.stdout.read(OUT_BUFFER_SIZE)
        return raw_out.decode().split()

    return reader

def _get_io_pair(process):
    return (_get_sender(process), _get_reader(process))

async def _open_ssh(where):
    ssh_proc = await asyncio.subprocess.create_subprocess_exec(
        'ssh',
        where,
        # f'ssh {where}',
        stdin=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )

    # ssh_proc = await asyncio.subprocess.create_subprocess_shell(
    #     f'ssh {where}',
    #     stdin=asyncio.subprocess.PIPE,
    #     stdout=asyncio.subprocess.PIPE,
    #     stderr=asyncio.subprocess.PIPE
    # )

    print(ssh_proc.pid)

    # killer = lambda: print(f'killing {where}') and ssh_proc.kill()
    def killer():
        ssh_proc.kill()

    return (*_get_io_pair(ssh_proc), killer)

class SSHProcess:
    def __init__(self, where):
        self.where = where
        self.reader = None
        self.writer = None
        self.killer = None

    async def _open(self):
        self.writer, self.reader, self.killer = await _open_ssh(self.where)

        return self

    async def send(self, msg: str):
        await self.writer(msg)

    async def receive(self):
        return await self.reader()

    async def communicate(self, msg: str=None):
        if msg:
            await self.send(msg)

        return await self.receive()

    def __del__(self):
        if self.killer:
            self.killer()

async def make(where):
    return await SSHProcess(where)._open()

async def bulk(*where) -> typing.List[SSHProcess]:
    return [await make(name) for name in where]