#!/usr/bin/python3
import os
import argparse

from pathlib import Path

root_dir = Path(__file__).parent

def get_args():
    parser = argparse.ArgumentParser(
        prog='TT bench data analyzer',
        description='Tool to analyze TT bench performance data'
    )

    parser.add_argument(
        'data file',
        help='Data file'
    )

    return(vars(parser.parse_args()))

def main():
    args = get_args()

    data_file_path = (root_dir / args['data file']).absolute()
    data_file = open(data_file_path, mode='r')

    print(data_file)

if __name__ == "__main__":
    main()
