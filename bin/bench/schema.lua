local fio = require('fio')
local schema_generator = require('schema_generator')

local function init_schema()
    box.schema.user.grant('guest', 'execute,read,write', 'universe', nil, {if_not_exists=true})

    local file = fio.open('schema.yml', 'O_RDONLY')
    local yaml_doc = file:read()
    local init_spaces = schema_generator(yaml_doc)
    init_spaces()
end

return {
    init_schema = init_schema,
}
