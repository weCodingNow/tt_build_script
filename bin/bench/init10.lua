--local metrics = require('metrics')
local http_router = require('http.router')
local http_server = require('http.server')
local http_handler = require('metrics.plugins.prometheus').collect_http


local metrics_fix = require('metrics_fix')
local schema = require('schema')
--local procedures = require('procedures')

local function init_c_procedures()
    box.schema.func.create('libmoex.get', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.get', { if_not_exists = true })

    box.schema.func.create('libmoex.push', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.push', { if_not_exists = true })

    box.schema.func.create('libmoex.update', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.update', { if_not_exists = true })

    box.schema.func.create('libmoex.get_by_index', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.get_by_index', { if_not_exists = true })

    box.schema.func.create('libmoex.get_traced', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.get_traced', { if_not_exists = true })

    box.schema.func.create('libmoex.get_by_index_traced', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.get_by_index_traced', { if_not_exists = true })

    box.schema.func.create('libmoex.push_traced', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.push_traced', { if_not_exists = true })

    box.schema.func.create('libmoex.update_traced', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.update_traced', { if_not_exists = true })

    box.schema.func.create('libmoex.bench', { language = 'C', if_not_exists = true })
    box.schema.user.grant('guest', 'execute', 'function', 'libmoex.bench', { if_not_exists = true })
end


local port = os.getenv('TARANTOOL_PORT')
if port == nil then
    port = '3301'
end

box.cfg{
    listen = '0.0.0.0:' .. port,
    --memtx_memory = 1 * 1024 * 1024 * 1024,
    --net_msg_max = 4096,
    readahead = 256 * 1024 * 1024,
    wal_mode = 'none',
}

schema.init_schema()
--rawset(_G, 'push', procedures.push)

init_c_procedures()

--metrics.enable_default_metrics()
metrics_fix.enable()
local httpd = http_server.new('0.0.0.0', 8088, {log_requests = false})
local router = http_router.new():route({path = '/metrics'}, http_handler)
httpd:set_router(router)
httpd:start()

-- Для дебага C-процедур через консоль
capi_connection = require('net.box'):new(port)

-- Гипотезы с бакетами

rawset(_G, 'buckets', require('buckets'))

box.session.on_disconnect(buckets.bucket_delete)

require('fiber').create(buckets.worker)

require('console').start()
