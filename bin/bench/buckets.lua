local log = require('log')
local fiber = require('fiber')

local channel = fiber.channel(32 * 1024)

local MAX_TUPLES = 300

local buckets_map = {}

local function bucket_delete()
    if buckets_map[box.session.id()] ~= nil then
        buckets_map[box.session.id()] = nil
        log.info('bucket_delete: ' .. box.session.id())
    end
end

local function bucket_create()
    buckets_map[box.session.id()] = {
        len = 0,
        tuples = {},
    }
    log.info('bucket_create: ' .. box.session.id())
end

local function push(space_id, tuple)
    channel:put(tuple)

    return { tuple }
end

local function get(space_id, key)
    local result = buckets_map[box.session.id()].tuples
    buckets_map[box.session.id()].tuples = {}
    buckets_map[box.session.id()].len = 0

    return result
end

local function worker()
    while true do
        local tuple = channel:get(1)
        if tuple ~= nil then
            for k, v in pairs(buckets_map) do
                if v.len < MAX_TUPLES then
                    table.insert(v.tuples, tuple)
                    v.len = v.len + 1
                end
            end
        end
    end
end

return {
    bucket_create = bucket_create,
    bucket_delete = bucket_delete,
    push = push,
    get = get,
    worker = worker,
}
