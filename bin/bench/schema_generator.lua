local yaml = require('yaml')

local function import_schema(yaml_doc)
    local schema_definition = yaml.decode(yaml_doc)

    return function()
        for name, space_definition in pairs(schema_definition.spaces) do
            local space = box.schema.space.create(name, space_definition.options)
            --space:format(space_definition.fields)

            for _, index_definition in ipairs(space_definition.indices) do
                space:create_index(index_definition.name, index_definition.options)
            end
        end
    end
end

return import_schema
