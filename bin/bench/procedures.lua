
local function push(space_id, tuple)
    local space = box.space[space_id]
    return space:insert(tuple)
end

return {
    push = push,
}
