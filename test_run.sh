VERSION="${1:-"1.10.5"}"

echo "starting build $VERSION"

pushd ansible > /dev/null

ansible-playbook 01-build_tt.yml --extra-vars "tt_ver=\"$VERSION\"" > /dev/null \

if [ -z "$?" ]; then
    echo "build success"
    else
    echo "epic fail"
    exit
fi

popd > /dev/null

echo "build succesful"
echo -e "test starting at\n$(date)"

# удаляем .snap-файл с предыдущего рана
ssh tt-build "find *.snap | xargs rm"

( \
    ssh tt-build "cd bench && tarantool init10.lua" \
    || ssh tt-build "cd bench && tarantool init_17.lua" \
) &

sleep 1
timeout 600 ssh tt-build ./bench/bench -w 8 2>&1 | tee
